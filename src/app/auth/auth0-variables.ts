interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: 'JCn9FCsbi7VyGvENM3edtn9Mc523pqp1',
  domain: 'pedo.eu.auth0.com',
  callbackURL: 'http://localhost:4200/callback'
};
