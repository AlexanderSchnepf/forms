import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistrierenComponent } from './registrieren/registrieren.component';
import { CallbackComponent } from './callback/callback.component';
const routes: Routes = [{
  path: 'login',
  component: LoginComponent
},
{
  path: 'registrieren',
  component: RegistrierenComponent
},
{
  path: 'callback',
  component: CallbackComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
