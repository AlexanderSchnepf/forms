import { Component, OnInit } from '@angular/core';
import { User } from './registrieren.user';
import { AbstractControl } from '@angular/forms';
@Component({
  selector: 'app-registrieren',
  templateUrl: './registrieren.component.html',
  styleUrls: ['./registrieren.component.scss']
})
export class RegistrierenComponent implements OnInit {

  private user: User;
  count
  constructor() {

  }

  ngOnInit() {
    this.user = new User({
      username: "", email: "", password: { pwd: "", confirm_pwd: "" }
    });
  }

  save(model: User) {
    sessionStorage.setItem(model.username, JSON.stringify(model));
    console.log(model);
    console.log(model.username);
    console.log("------------------");
    //console.log("User was added! Try to log in!");

    for (let i = 0; i < sessionStorage.length; i++) {
      let key = sessionStorage.key(i);
      let value = sessionStorage.getItem(key);
      console.log(key, value);
    }
      setTimeout(function () {
        location.href = "/login";
      }, 5000);
    }
  }


